﻿using DesertCamel.BaseMicroservices.SuperBootstrap.Core;

namespace DesertCamel.BaseMicroservices.SuperIdentity.Models.PermissionService
{
    public class PermissionListResponseModel : FuncListResponse<PermissionGetResponseModel>
    {
    }
}
