﻿using DesertCamel.BaseMicroservices.SuperBootstrap.Core;

namespace DesertCamel.BaseMicroservices.SuperIdentity.Models.RoleService
{
    public class RoleListResponseModel : FuncListResponse<RoleGetResponseModel>
    {
    }
}
