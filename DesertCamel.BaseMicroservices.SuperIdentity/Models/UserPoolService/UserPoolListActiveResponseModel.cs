﻿using DesertCamel.BaseMicroservices.SuperBootstrap.Core;

namespace DesertCamel.BaseMicroservices.SuperIdentity.Models.UserPoolService
{
    public class UserPoolListActiveResponseModel : FuncListResponse<UserPoolListActiveItemResponseModel>
    {
    }
}
